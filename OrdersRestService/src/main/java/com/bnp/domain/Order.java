package com.bnp.domain;

import java.util.List;

public class Order {
	
	private String orderId;
	private List<OrderItem> orderItems;
	private String customerId;
	
	public Order(String orderId, List<OrderItem> orderItems, String customerId) {
		super();
		this.orderId = orderId;
		this.orderItems = orderItems;
		this.customerId = customerId;
	}
	
	public String getOrderId() {
		return orderId;
	}
	
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public List<OrderItem> getOrderItems() {
		return orderItems;
	}
	
	public void setOrderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	
	

}
