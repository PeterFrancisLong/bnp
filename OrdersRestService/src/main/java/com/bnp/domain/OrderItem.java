package com.bnp.domain;

public class OrderItem {
	private String idemId;
	private Integer quantity;
	public String getIdemId() {
		return idemId;
	}
	public void setIdemId(String idemId) {
		this.idemId = idemId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public OrderItem(String idemId, Integer quantity) {
		super();
		this.idemId = idemId;
		this.quantity = quantity;
	}
	
	

}
