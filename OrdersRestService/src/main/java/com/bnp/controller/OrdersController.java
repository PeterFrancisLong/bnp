package com.bnp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bnp.domain.Order;
import com.bnp.service.OrderService;

@RestController
@RequestMapping("/orders")
public class OrdersController {
	
	@Autowired
	private OrderService orderService;

	@RequestMapping(method = RequestMethod.GET)
	public List<Order> orders() {
		return orderService.getOrders();
	}

	@RequestMapping(method = RequestMethod.DELETE)
	String delete() {
		return "Deleted some orders";
	}

	@RequestMapping(method = RequestMethod.POST)
	String post() {
		return "Posted some orders";
	}

	@RequestMapping(method = RequestMethod.PUT)
	String put() {
		return "Put some orders";
	}

	@RequestMapping(method = RequestMethod.PATCH)
	String patch() {
		return "Patch some orders";
	}

}
