package com.bnp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.bnp.domain.Order;

@Service
public class OrderService {
	
	private List<Order> orders = new ArrayList<>();
	
	public void deleteOrder(Order order) {
		orders.remove(order);		
	}

	public List<Order> getOrders() {
		return orders;
	}

	

}
