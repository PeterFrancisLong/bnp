package com.bnp.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.bnp.configuration.Application;
import com.bnp.domain.Order;
import com.bnp.domain.OrderItem;
import com.bnp.service.OrderService;

@RunWith(SpringRunner.class)
@WebMvcTest
@ContextConfiguration(classes={Application.class})
public class MvcOrdersControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private OrderService service;

  private List<Order> orders;
  
 @Before
 public void setUp() {
	 orders = new ArrayList<>();
	 OrderItem orderItem = new OrderItem("1234", 4);
	 List<OrderItem> orderItems = new ArrayList<>();
	 orderItems.add(orderItem);
	 
	 Order firstOrder = new Order("111", orderItems, "789");
	 orders.add(firstOrder);
 }
  
  @Test
  public void getOrders() throws Exception {
    when(service.getOrders()).thenReturn(orders);
	  this.mockMvc
    	.perform(get("/orders"))
    	.andDo(print())
    	.andExpect(status().isOk())
        .andExpect(content().json("[{\"orderId\":\"111\",\"orderItems\":[{\"idemId\":\"1234\",\"quantity\":4}],\"customerId\":\"789\"}]"));
  }
  
  @Test
  public void deleteOrder() throws Exception {
    this.mockMvc
    	.perform(delete("/orders"))
    	.andDo(print())
    	.andExpect(status().isOk())
        .andExpect(content().json("[{\"orderId\":\"111\",\"orderItems\":[{\"idemId\":\"1234\",\"quantity\":4}],\"customerId\":\"789\"}]"));
  }
  
  @Test
  public void postOrder() throws Exception {
    this.mockMvc
    	.perform(post("/orders"))
    	.andDo(print())
    	.andExpect(status().isOk())
        .andExpect(content().string(containsString("Posted some orders")));
  }
  
  @Test
  public void putOrder() throws Exception {
    this.mockMvc
    	.perform(put("/orders"))
    	.andDo(print())
    	.andExpect(status().isOk())
        .andExpect(content().string(containsString("Put some orders")));
  }
  
  @Test
  public void patchOrder() throws Exception {
    this.mockMvc
    	.perform(patch("/orders"))
    	.andDo(print())
    	.andExpect(status().isOk())
        .andExpect(content().string(containsString("Patch some orders")));
  }
  
  
}
