# README #

This is a simple spring boot application that exposes a rest service for handling Order processing. 
The application can be run as a spring boot app from OrdersRestService using 

"mvn spring-boot:run"

This command will expose a rest service on port 8080. The service implements endpoints for Get, Delete, POST, Put and patch. These endpoints are only placeholders at the moment.

MVC tests have been implemented for these placeholder methods and the get method test has a mocked JSON response.


